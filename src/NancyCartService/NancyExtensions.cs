﻿using System;
using Nancy;

namespace NancyCartService
{
    public static class NancyExtensions
    {
        public static bool IsPost(this Request request)
        {
            return RequestMethodMatches(request, "POST");
        }
        
        private static bool RequestMethodMatches(Request request, string expectedMethod)
        {
            return string.Compare(request.Method, expectedMethod, StringComparison.OrdinalIgnoreCase) == 0;
        }
    }
}
