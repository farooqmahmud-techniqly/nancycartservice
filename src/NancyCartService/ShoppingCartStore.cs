﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NancyCartService.Models;

namespace NancyCartService
{
    public class ShoppingCartStore : IShoppingCartStore
    {
        private readonly IEventStore _eventStore;
        private Cart _cart;

        public ShoppingCartStore(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public async Task<Cart> GetCartAsync(int userId)
        {
            if (userId != 100)
            {
                return null;
            }

            if (_cart != null)
            {
                return _cart;
            }

           _cart = new Cart(_eventStore) {Id = 1, UserId = userId};
            return _cart;
        }

        public Task<int> SaveAsync(Cart cart)
        {
            _cart = cart;
            return Task.FromResult(1);
        }
    }
}