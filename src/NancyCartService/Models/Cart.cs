﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NancyCartService.Models
{
    public class Cart
    {
        private readonly IEventStore _eventStore;
        private readonly List<Product> _products = new List<Product>();

        public Cart(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public int Id { get; set; }
        public int UserId { get; set; }

        public IEnumerable<Product> CartItems => _products;

        public void AddItems(IEnumerable<Product> products)
        {
            _eventStore.Raise("ShoppingCart-ItemsAdded", products.Select(p => new {UserId, p.Id, Added = DateTime.UtcNow}));
            _products.AddRange(products);
        }
    }
}
