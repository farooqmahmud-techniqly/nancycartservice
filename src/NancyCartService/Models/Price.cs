﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyCartService.Models
{
    public class Price
    {
        public string Currency { get; set; }
        public decimal Value { get; set; }
    }
}
