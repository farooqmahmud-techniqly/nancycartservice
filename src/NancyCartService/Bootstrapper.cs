﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses.Negotiation;
using Nancy.TinyIoc;

namespace NancyCartService
{
    public sealed class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(requestContainer, pipelines, context);

            pipelines.BeforeRequest += c =>
                                       {
                                           var headers = c.Request.Headers;

                                           if (!IsAcceptHeadersAllowed(headers.Accept))
                                           {
                                               return new Response { StatusCode = HttpStatusCode.NotAcceptable };
                                           }

                                           if (c.Request.IsPost())
                                           {
                                               if (!ContentTypeAllowed(headers.ContentType))
                                               {
                                                   return new Response {StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = "'application/json' Content-Type must be specified in the header."};
                                               }
                                           }

                                           return null;
                                       };

            pipelines.AfterRequest += c =>
                                      {
                                          if (c.Request.IsPost() && c.Response.StatusCode == HttpStatusCode.OK)
                                          {
                                              c.Response.StatusCode = HttpStatusCode.Created;
                                          }
                                      };
        }

        private bool ContentTypeAllowed(MediaRange contentType)
        {
            return contentType != null &&  contentType.Matches("application/json");
        }

        private bool IsAcceptHeadersAllowed(IEnumerable<Tuple<string, decimal>> acceptTypes)
        {
            return acceptTypes.Any(tuple =>
            {
                var accept = new MediaRange(tuple.Item1);
                return accept.Matches("application/json");
            });
        }
    }
}
