﻿using System.Collections.Generic;

namespace NancyCartService
{
    public interface IEventStore
    {
        void Raise(string eventName, IEnumerable<object> eventData);
    }
}
