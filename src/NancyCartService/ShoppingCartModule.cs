﻿using Nancy;
using Nancy.ModelBinding;

namespace NancyCartService
{
    public sealed class ShoppingCartModule : NancyModule
    {
        public ShoppingCartModule(IShoppingCartStore shoppingCartStore, IProductService productService) : base("/cart")
        {
            Get("/{userId:int}", async(parameters) =>
            {
                var userId = (int) parameters.userId;
                var cart = await shoppingCartStore.GetCartAsync(userId);

                if (cart == null)
                {
                    return HttpStatusCode.NotFound;
                }

                return cart;
            });

            Post("/{userId:int}/items",
                async (parameters) =>
                      {
                          var productIds = this.Bind<int[]>(new BindingConfig {IgnoreErrors = true});

                          if (productIds.Length == 0)
                          {
                              return new Response {StatusCode = HttpStatusCode.BadRequest, ReasonPhrase = "A list of product id's must be specified."};
                          }

                          var products = await productService.GetProductsAsync(productIds);

                          var userId = (int)parameters.userId;
                          var cart = await shoppingCartStore.GetCartAsync(userId);
                          cart.AddItems(products);

                          await shoppingCartStore.SaveAsync(cart);
                          return cart;
                      });
        }
    }
}