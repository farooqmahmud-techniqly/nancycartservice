﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NancyCartService.Models;

namespace NancyCartService
{
    public sealed class ProductService : IProductService
    {
        private  readonly List<Product> _products;

        public ProductService()
        {
            _products = CreateDummyProducts();
        }

        private List<Product> CreateDummyProducts()
        {
            return new List<Product>
                   {
                       new Product
                       {
                           Id = 20000,
                           Description = "Basketball shoes",
                           Name = "Nike Air Jordan XI",
                           Price = new Price {Currency = "USD", Value = 199.99M}
                       },
                       new Product
                       {
                           Id = 20001,
                           Description = "Basketball shorts",
                           Name = "Nike Basketball Shorts",
                           Price = new Price {Currency = "USD", Value = 39.99M}
                       }
                   };
        }

        public Task<IEnumerable<Product>> GetProductsAsync(IEnumerable<int> productIds)
        {
            return Task.FromResult(_products.AsEnumerable());
        }
    }
}
