﻿using System.Threading.Tasks;
using NancyCartService.Models;

namespace NancyCartService
{
    public interface IShoppingCartStore
    {
        Task<Cart> GetCartAsync(int userId);
        Task<int> SaveAsync(Cart cart);
    }
}
