﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NancyCartService.Models;

namespace NancyCartService
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetProductsAsync(IEnumerable<int> productIds);
    }
}
