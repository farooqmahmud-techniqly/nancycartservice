﻿namespace NancyCartService.Tests
{
    public sealed class BootstrapperFixture
    {
        public Bootstrapper BootStrapper { get; }

        public BootstrapperFixture()
        {
            BootStrapper = new Bootstrapper();
        }
    }
}
