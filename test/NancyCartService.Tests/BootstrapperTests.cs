﻿using System.Threading.Tasks;
using Nancy;
using Nancy.Testing;
using Xunit;

namespace NancyCartService.Tests
{
    public class BootstrapperTests : IClassFixture<BootstrapperFixture>
    {
        private readonly BootstrapperFixture _bootstrapperFixture;

        public BootstrapperTests(BootstrapperFixture bootstrapperFixture)
        {
            _bootstrapperFixture = bootstrapperFixture;
        }

        [Fact]
        public async Task Request_WhenAcceptHeaderMissing_ReturnsStatusNotAcceptable()
        {
            var browser = new Browser(_bootstrapperFixture.BootStrapper,
                (to) =>
                {
                    to.Header("Content-Type", "application/json");
                });

            var result = await browser.Get("/cart/100", with => with.HttpRequest());
            Assert.Equal(HttpStatusCode.NotAcceptable, result.StatusCode);
        }

        [Fact]
        public async Task Request_WhenPost_AndContentTypeHeaderMissing_ReturnsStatusBadRequest_WithReasonPhrase()
        {
            var browser = new Browser(_bootstrapperFixture.BootStrapper,
                (to) =>
                {
                    to.Accept("application/json");
                });

            var result = await browser.Post("/cart/100/items", with => with.HttpRequest());
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.False(string.IsNullOrWhiteSpace(result.ReasonPhrase));
        }

    }
}
