﻿using System.Collections.Generic;
using Moq;
using NancyCartService.Models;
using Xunit;

namespace NancyCartService.Tests.Models
{
    public class CartTests
    {
        [Fact]
        public void AddItems_RaisesItemAddedEvent_ForEachProductAddedToCart()
        {
            var eventStoreMock = new Mock<IEventStore>();
            var cart = new Cart(eventStoreMock.Object);

            var products = new List<Product>
                           {
                               new Product {Id = 1},
                               new Product {Id = 2}
                           };

            cart.AddItems(products);
            eventStoreMock.Verify(e => e.Raise("ShoppingCart-ItemsAdded", It.IsAny<IEnumerable<object>>()), Times.Once);
        }
    }
}
