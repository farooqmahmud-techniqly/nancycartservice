﻿using System.Threading.Tasks;
using Nancy;
using Nancy.Testing;
using NancyCartService.Models;
using Newtonsoft.Json;
using Xunit;

namespace NancyCartService.Tests
{
    public class ShoppingCartModuleTests : IClassFixture<BrowserFixture>
    {
        private readonly BrowserFixture _browserFixture;

        public ShoppingCartModuleTests(BrowserFixture browserFixture)
        {
            _browserFixture = browserFixture;
        }

        [Fact]
        public async Task Get_WhenCartExists_ReturnsStatusOk()
        {
            var response = await _browserFixture.Browser.Get("/cart/100", with => with.HttpRequest());
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task Get_WhenCartExists_ReturnsTheCart()
        {
            var response = await _browserFixture.Browser.Get("/cart/100", with => with.HttpRequest());
            var cart = JsonConvert.DeserializeObject<Cart>(response.Body.AsString());
            Assert.Equal(100, cart.UserId);
        }

        [Fact]
        public async Task Get_WhenCartDoesNotExist_ReturnsStatusNotFound()
        {
            var response = await _browserFixture.Browser.Get("/cart/1000", with => with.HttpRequest());
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Post_WhenItemsAdded_ReturnsStatusCreated()
        {
            var productIds = new[] {1, 2, 3, 4, 5};
             
            var response = await _browserFixture.Browser.Post("/cart/100/items",
                (with) =>
                {
                    with.HttpRequest();
                    with.Body(JsonConvert.SerializeObject(productIds));
                });

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task Post_WhenItemsAdded_ReturnsTheCart()
        {
            var productIds = new[] { 1, 2, 3, 4, 5 };

            var response = await _browserFixture.Browser.Post("/cart/100/items",
                (with) =>
                {
                    with.HttpRequest();
                    with.Body(JsonConvert.SerializeObject(productIds));
                });

            var cart = JsonConvert.DeserializeObject<Cart>(response.Body.AsString());
            Assert.Equal(100, cart.UserId);
        }

        [Fact]
        public async Task Post_WhenProductIdsNotSpecified_ReturnsStatusBadRequestWithReasonPhrase()
        {
            var response = await _browserFixture.Browser.Post("/cart/100/items",
                (with) =>
                {
                    with.HttpRequest();
                });

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.False(string.IsNullOrWhiteSpace(response.ReasonPhrase));
        }
    }
}
