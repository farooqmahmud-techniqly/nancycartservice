﻿using Nancy.Testing;

namespace NancyCartService.Tests
{
    public sealed class BrowserFixture
    {
        public Browser Browser { get; }

        public BrowserFixture()
        {
            var bs = new Bootstrapper();
            Browser = new Browser(bs,
                (to) =>
                {
                    to.Accept("application/json");
                    to.Header("Content-Type", "application/json");
                });
        }
    }
}
